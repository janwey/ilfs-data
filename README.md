# ILFS-Data

> Tools to collect, analyze and visualize data about participation in the
> [I Love Free Software Campaign](https://ilovefs.org) For further information
> refer to the [documentation](./docs/README.md).

* Free Software: GPL-3.0-or-later

* Libre Graphics: CC-BY-4.0

* Documentation: <https://git.fsfe.org/janwey/ilfs-data/src/branch/master/docs/README.md>

* Source code: <https://git.fsfe.org/janwey/ilfs-data>

* R: 3.5.2

## Maintainers

* Jan Weymeirsch - janwey at fsfe dot org

## Contribute

Any pull requests or suggestions are welcome at
<https://git.fsfe.org/janwey/ilfs-data> or via e-mail to one of the maintainers.

As of now, the [documentation](./docs/README.md) needs some maintainance :)

## License

Copyright (C) 2018 - 2019 Free Software Foundation Europe e.V.

Licensed under the GNU General Public License version 3.0, graphics and plots
are licensed under Creative Commons Attribution 4.0 International Public License

## Mentions

* First time usage during [ILFS Day](https://ilovefs.org/) 2018 in:

    * [#ilovefs Report 2018](https://fsfe.org/news/2018/news-20180308-01.html)
    
    * [Annual Report 2018](https://fsfe.org/news/2018/news-20181105-01.html#ilovefs)

## Examples: Plots

**2021**

![](./plots/participation_2021_1.png)

![](./plots/participation_2021_2.png)

![](./plots/timeplot_2021.png)

![](./plots/wordcloud_2021.png)


**2020**

![](./plots/participation_2020_1.png)

![](./plots/participation_2020_2.png)

![](./plots/timeplot_2020.png)

![](./plots/wordcloud_2020.png)


**2019**

![](./plots/participation_2019_1.png)

![](./plots/participation_2019_2.png)

![](./plots/timeplot_2019.png)

![](./plots/wordcloud_2019.png)

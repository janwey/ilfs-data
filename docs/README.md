# Documentation

* **[collecto.R](./collector.md)** the collector/scraper for data from different socialmedia-sources
* **[plotte.R](./plotter.md)** the visualizer script for the collected data
* **[word_cloud.py](./wordcloud.md)** another visualizer script, generating a wordcloud graphic
# Documentation: [collecto.R](../scripts/collecto.R)

## Table of Contents

* [General information about the Script](#the-script)
* [Packages and Functions used for Scraping](#packages-and-functions)
    * [The rtweet package](#the-twitter-package)
    * [The curl and rjson packages](#the-curl-and-rjson-packages)
    * [Custom functions for scraping](#custom-functions)
    * [The RedditExtractoR package](#the-redditextractor-package)
* [Collecting from Twitter](#twitter)
* [Collecting from the Fediverse](#fediverse)
* [Collecting from Reddit](#reddit)
* [Exporting the Datasets](#exporting-datasets)


* * *

## The Script

The R script documented here has a modular structure. It is divided into several
sections that handle loading the packages necessary for the process and
exporting the aggregated data into usable formats in the end. The remaining
sections handle one specific data source each (eg.: Twitter, Mastodon, Reddit).
While the [Package-Section](#packages) is obviously necessary for the remaining
sections (depending on which ones you actually want to use) as well as the
[Export-Section](#exporting) for actually using the data in other applications,
scripts or by other people, you can cherry-pick between the
[Datasource-Sections](#datasources). These can be used independently and in no
particular order to another. Keep that in mind, if you only want to analyze *X*.

As a site-note, the script is written to keep the data collected as anonymous as
possible, however because one deals with a rather small sample and because of
the nature of social media, it is in most cases still possible to disclose
specific observations in the resulting data. As only public postings is
accessed, it is safe to assume, that people want their posts to be seen anyways,
so it is not as problematic as it may seem. Nevertheless, one should still treat
the data with much care and do not leak meta-data if possible.

* * *

## Packages and Functions

As of writing this script and its documentation, two platform specific and two
general scraper-packages are being used, as well as three packages required for
exporting to foreign datatypes and manipulating/cleaning data.

* [rtweet](https://cran.r-project.org/package=rtweet) (Version 0.6.7)
* [curl](https://cran.r-project.org/package=curl) (Version 3.2)
* [rjson](https://cran.r-project.org/package=rjson) (Version 0.2.20)
* [RedditExtractoR](https://cran.r-project.org/package=RedditExtractoR) (Version
  2.1.5)
* [readODS](https://cran.r-project.org/package=readODS) (Version 1.6.7)
* [stringi](https://cran.r-project.org/package=stringi) (Version 1.2.4)
* [tidyr](https://cran.r-project.org/package=tidyr) (Version 0.8.1)

These are installed if necessary and loaded accordingly with a little
*trickery*:

```{.R}
  if (!require("package")) {
    install.packages("package")
    library("package")
  }
```

This particular chunk of code tries to load the library "package" via the
`require()` function, which if it fails does not exit the process as `library()`
would do. In case it does fail, `require()` returns a `FALSE` value to the
console, which one can revert to a `TRUE` statement by prefix-ing `!`. In this
case, the if-statement runs the code inside its expression. When loading the
package via `require()` fails, the supposed reason is, that the package is not
yet installed, so `install.packages()` will do exactly that, followed by
`library()` to load the newly installed package. This chunk of code may be
transformed into a *1-liner* easily:

```{.R}
  if (!require("package")) install.packages("package"); library("package")
```

### The rtweet package

rtweet has a rather extensive
[documentation](https://cran.r-project.org/web/packages/rtweet/rtweet.pdf) as
well as "in-R-manuals". Simply entering `??rtweet` into the R console or looking
up a specific function with `?function`, replacing `function` with its actual
name prints out sufficient manual pages. The package is the successor of the
previously used [twittR](https://cran.r-project.org/package=rtweet) package with
a lot of improvements and fewer restrictions regarding the Twitter-API. rtweet
has several useful functions to scrape Twitter-Data, most of which however apply
to the Twitter account in use - which in our case is not necessary. It is
noteworthy, that searches of tweets are restricted to only a few days into the
past, making it impossible to scrape old tweets from - for example - last
year. The [Twitter-Section](#twitter) uses only two functions, which will be
discussed individually, later on:

```{.R}
  create_token()  # authentication
  search_tweets() # searching Twitter for a particular string
```

As a site-note; [httr-package](https://cran.r-project.org/package=httr) - a
dependency of rtweet - had to be installed as a local package of the OS'
packagemanager, as the one provided by CRAN would not compile properly.  So if
similar difficulties occur, a look for something like `r-cran-httr` in the
packagemanager could be useful.

### The curl and rjson packages

One convenient aspect of Mastodon is, that searches are not restricted to a
single Mastodon-Instance or to Mastodon at all. If the instance of choice has
enough outbound connections (an analyst should make sure to chose a very active
and inter-communicative noce), one is able to not only search
Mastodon-Instances, but also GNUsocial, Pump.io and other compatible Social
Media instances in the fediverse. Previously, a specialized package for scraping
the Fediverse was used, simply called
[mastodon](https://github.com/ThomasChln/mastodon), however it appeared to be
unreliable, poorly documented and possibly even unmaintained. Fortunately,
Mastodon itself as a platform built as Free Software has a convenient open API
that is accessible with simple tools like
[curl](https://cran.r-project.org/package=curl) and
[rjson](https://cran.r-project.org/package=rjson). Specifically, this script
uses following functions of the `curl`{.R} package:

```{.R}
  curl_fetch_memory() # import HTTP headers from the API-page
  parse_headers()     # extracts responses from the HTTP header
```

This will generate an output in a JSON format, which can be transformed to a
`list()` item with a function from the `rjson` package:

```{.R}
  fromJSON() # transform JSON to a list() item
```

All of this happens exclusively in the [`mastodon.search()`](#mastodonsearch)
custom function. However, when adapting its code, knowledge about the scraping
functionality via `curl` and `rjson` is necessary.

### Custom Functions

During the early stages of the script, another external R-file `functions.R` is
sourced. It contains various helper-functions used to scrape and manipulate
data. This sections discusses each of them briefly.

#### list2vec

`list2vec()` is a rather simple helper-function which transforms a given input
`list()` object into a single string with every former item of the `list()`
separated by some specified character (by default: ","). Given an nested
`list()` object such as shown below, the function uses `unlist()` to transform
it into a vector with the same length as the former `list()`. The function
`paste()` with the `collapse` argument is used to join the list into a single
string, separated by the given character.

Running `list2vec()` on a list, such as this:

```
  my_object
  |
  |- post_1
  |  |
  |  |- "#ilovefs"
  |  '- "#freesoftware"
  |
  |- post_2
  |  |
  |  |- "#ilovefs"
  |  '- "#fsfe"
 ...
```

Results in an object such as:

```
  my_object
  |
  |- post_1
  |  |
  |  '- "#ilovefs,#freesoftware"
  |
  |- post_2
  |  |
  |  '- "#ilovefs,#fsfe"
 ...
```

The reason for this may not yet be apparent, however in order to later transform
all data to a `data.frame()` with a single row for each observation,
multi-valued characteristics have to be joined. The internal use of the
`apply()` family in this script, instead of loops makes it reasonably fast even
for extensive data, due to vectorization.

This simple helper function is used frequently across the `collecto.R` script
and other custom functions.

#### valifexst

The `valifexst()` function stems from the need to mark *empty* value fields in
the data as `NA` (Not Available, the "missing data" indicator in
R). Occasionally, when empty strings are returned by - for example - `curl`, the
according cell contains the value `NULL`, which means it is not missing, just
empty. In order to overcome this issue, the `valifexts()` function uses
`ifelse()` internally to assign *empty* values the `NA` indicator. This works
both for single values, as well as vectors. The later should be used whenever
possible when massive amounts of data have to be treated. If other objects shall
be treated, the combination with the `apply()` function family serves well.

Example:

```
  # original vector
  "1"  "4"  ""  "3"  "10"  "2"  NULL  "16"  "9"
  
  # after use of valifexst()
  "1"  "4"  NA  "3"  "10"  "2"  NA  "16"  "9"
```

#### mastodon.extract

This internal function extracts *relevant* information from the fediverse data
as returned by `fromJSON()`, when translating the raw downloaded data into an
R-readable format. `mastodon.extract()` is called internally by
[`mastodon.search()`](#mastodonsearch), however, if there exists a `list()`
object with the same properties and structure, this function could be used
directly.

The current structure of the data, as returned by the API call is a nested
`list()` item, with each post (in Mastodon referred to as "toot") resting in its
own second level `list()`. In order to apply the extraction of the data to all
postings in an efficient manner, `sapply()` is used. Within each second level
`list()` (or for every "toot" data), time & date are extracted with the
regex-functions `gsub()` & `sub()` respectively. The former is also used to
*sanitize* the actual post content / text, by removing HTML tags and doubled
whitespace. The custom [`valifexst()`](#valifexst) function is used to *clean*
several variables regarding their "Availability". The [`list2vec()`](#list2vec)
function is used to force several hashtags used in the post into a single
string. A combination of both functions is used to extract media URLs (this was
not necessary for the hashtags, as at list one tag
- the one searched for - exists in here either way). Finally, the extracted data
of all posts is returned in a `data.frame()` with 12 variables, representing
exactly one characteristic for each observation / post.

#### mastodon.search

The `mastodon.search()` function is a simple wrapper around `curl`, `rjson`
packages and the [`mastodon.extract()`](#mastodonextract) function.

As Arguments, it takes the maximum iterations to run through the search-results
of a given search parameter, 40 posts at a time. If `maxiter = 10` is chosen, a
maximum of 400 posts will be returned. This value can be set to an arbitrary
high number, as the function will stop either way, in case no posts are found
anymore. However, this gives the possibility to only search for the *X* newest
posts containing a particular search term. By default, this is set to
`9999`. Secondly, a search term `hashtag` has to be assigned. This single-valued
`character()` object (without leading "#") is used to search the
fediverse. Lastly, another `character()` value for the instance/node can be
assigned (full URL including protocol). By default, this function uses `instance
= "https://mastodon.social"`.

The iterative process works as follows:

1. The function builds its initial URL for the API call, according to the given
   parameters.

2. The call is executed with `curl_fetch_memory()`, translated to the JSON
   format with `rawToChar()` and finally transformed to a nested `list()` object
   with `fromJSON()`.

3. If the downloaded data is *valid*, as in if there is actual data, the custom
   [`mastodon.extract()`](#mastodonextract) function extracts relevant
   information. Existing data (of the last iteration-run) is merged with it.

4. The API call URL is updated to fetch the next 40 posts.

Steps 2. - 4. are repeated as long as the validating-process in 3. succeeds. In
case of failure (there is no more data to be fetched) or if the `maxiter` value
is reached, the iterative process stops and the data is returned.

### The RedditExtractoR package

RedditExtractoR has a rather extensive
[documentation](https://cran.r-project.org/web/packages/RedditExtractoR/RedditExtractoR.pdf)
but no general "in-R-manual". One can however look up a specific function within
the package by entering `?function` into the R-prompt, replacing `function` with
its actual name. RedditExtractoR has several useful function to scrape
Reddit-Posts or create fancy graphs from them. In the case of this script, only
two basic functions are used and discussed in the [Reddit-Section](#reddit)
later on:

```{.R}
  reddit_urls()	        # searching Reddit for a particular string
  reddit_content()      # scrape data of an indicidual post
```

The observant reader may notice, that there is no "authenticate" command within
this package. As of now, the Reddit-API does not require authentication, as all
posts are for general consumption anyways. This may or may not change in the
future, so an eye has to be kept on this, if problems arise.

* * *

## Twitter

### Authenticate

As the package in use here needs access to the Twitter-API, what is needed first
are the "Consumer Key", "Consumer Secret" and our "App Name", all of which can
be created at [apps.twitter.com](https://apps.twitter.com/). Of course, for a
Twitter-Account is needed for this step (staff may ask for the FSFE's Account).

The authentication can most conveniently be done via a plain text file with the
saved credentials. This `.txt` file has a very specific structure which the
script expects. An example file can be found in the `examples/` folder. Going
this route can potentially be a security risk for the twitter-account in use, as
the `.txt` file is stored in plain text. The problem can be mitigated if the
storage drive is encrypted. *It may also be possible to implement decryption of
a file via GNUPG with the `system()` command. However, this has not been
implemented in this script (yet)*.

The first line of the credential-file contains the *labels*. These have to be in
the same order as the *credentials* themselves in the line below. The *labels*
as well as the *credentials* are each separated by a single semi-colon `;`.
Storing the credentials in plain text surely is not optimal, but the easiest way
to get the information into the R-Session.

Next, an authenticate token "oauth" with `create_token()` is generated. The
oauth token can not only be used to scrape information from Twitter, it also
grants write-access, so can be used to manipulate the affiliated Twitter-Account
or interact with Twitter in any other way. This has to be kept in mind, when
storing these credentials in plain text, like this.

The function used to authenticate takes the consumer-key and consumer-secret as
well as the name of the app, which was registered on the twitter-developer page
before, as arguments. As mentioned before, it was imported via the plain text
file and stored in the `consumer_key consumer_private appname` variables within
the `tw_cred` dataframe:

```{.R}
  twitter_token <- create_token(app = tw_cred$appname,
                                consumer_key = tw_cred$consumer_key,
                                consumer_secret = tw_cred$consumer_private)
```

### Scraping Tweets

Once the oauth token has been created, the desired tweets can be collected right
away with the `search_tweets()` function. All functions in the `rtweet` package
access the token via environment variables. However, other arguments have to be
forwarded to the according functions:

* the string to search for, in this case `"ilovefs"`. This will not only include
  things like "ilovefs18", "ilovefs2018", "ILoveFS", etc but also hashtags like
  "#ilovefs"
  
* the maximum number of tweets to be aggregated. This number is only useful for
  search-terms that get a lot of coverage on twitter (eg.: trending hashtags).
  For the purpose of this particular script, it can safely be set to a number
  that is much higher than the anticipated participation in the campaign, e.g.
  `9999999999` so **all** tweets containing the specified string will be
  collected.
  
* Lastly, a binary indicate on whether to include retweets in the data can be
  given (for the case of this script, it is set to `FALSE`).

The result of this function is being stored in the variable `tweets`. The
appropriate code is:

```{.R}
  tweets <- search_tweets(q = "ilovefs",
                          n = 9999,
                          include_rts = FALSE)
```

*Note: A restriction of variables to be stored can be done with the indexing
parenthesis `[row, col ]`. All data will still be scraped, but only the
specified variables (columns) stored in the `tweets` variable. This way, a lot
of data-cleaning can be done directly on the spot:*

```{.R}
tweets <- search_tweets(q = "ilovefs",
                        n = 9999,
                        include_rts = FALSE)[,
    c("user_id", "created_at", "text", "source", "favorite_count",
      "retweet_count", "hashtags", "urls_expanded_url", "media_expanded_url",
      "ext_media_expanded_url", "lang", "location", "status_url", "protected")]
```

### Data Cleaning

Most of the resulting data can be accessed with simple assignment, only a few
characteristics are organized within second level `list()` items in the
dataset. The structure of the dataset is as follows:

```
  tweets
    |
    |- ...
    |- text = "Yay! #ilovefs", "Today is #ilovefs, celebrate!", ...
    |- ...
    |- screen_name = "user123", "fsfe", ...
    |- ...
    |- source = "Twidere", "Tweetdeck", ...
    |- ...
    |- favorite_count = 8, 11, ...
    |- retweet_count = 2, 7, ...
    |- ...
    |- lang = "en", "en", ...
    |- ...
    |- created_at = "14-02-2018" 10:01 CET", "14-02-2018 10:01 CET", ...
    |- ...
    |- urls_expanded_url
    |    |- NA, "https://ilovefs.org", ...
    |    '- ...
    |
    |- media_expanded_url
    |    |- NA, NA, ...
    |    '- ...
    |
    '- ...
```

Fortunately, with the help of the `within()`, as well as the previously
discussed custom [`list2vec()`](#list2vec) functions, the data can be cleaned
easily. The `within()` function thereby is mere convenience. It takes a single
dataset as input, as well as expressions to operate *within* the dataset. This
saves the user from repeatedly using the prefix `tweets$` in front of every
action. Instead of `tweets$user_id`, while operating with the `within()`
function, simply `user_id` can be used.

This is used for cleaning and re-coding the dataset where needed. The `user_id`
Variable is re-coded to `user`. Additionally, instead of containing the username
of the post, it contains a numeric value unique only to this very dataset and
thus effectively anonymizing this entry as far as possible with such simple
methods:

```{.R}
# Original "user_id" variable:
"@fsfe", "@fsf", "@fsfe", "@user123", "@fsf", ...

# Re-coded "user" variable:
1, 2, 1, 3, 2, ...
```

The original `created_at` variable is split into `time` & `date` with the help
of the `sub()` function and some simple regex. Similarly, `text`, containing the
written content of each tweet is sanitized by removing all URLs and linebreaks
with `gsub()`. Mentioned accounts are extracted with stringi's
`stri_extract_all()`. Its output is yet again a `list()` object, which is
transformed to a single string-variable with the custom
[`list2vec()`](#list2vec) function.

*If not familiar with regex, [regexr.com](https://regexr.com/) is highly
recommended to learn how to use it. It also contains a nifty cheat-sheet.*

Values already stored in second level `list()`s such as `hashtags`,
`urls_expanded_url`, `media_expanded_url` & `ext_media_expanded_url` are
transformed to single strings with [`list2vec()`](#list2vec) and stored in
`htag`, `urls`, `murl` & `mext` accordingly.

Other variables, such as `favorite_count`, `retweet_count`, `status_url`,
`location` & `source` are simply renamed to `favs`, `retw`, `link`, `posi`, &
`clnt`.

Lastly, it makes sense to restrict the data to a specific time frame (for
example, in order to "filter out" #iLoveFS promotion prior to February
14th). This process can be done with the parenthesis indexing again, using a
logic operation within:

```{.R}
  tweets <- tweets[(as.Date(tweets$date) > as.Date("2019-02-11") &
                    as.Date(tweets$date) < as.Date("2019-02-17")),]
```

The `tweets$date` vector is formed to a date object by `as.Date()` and being
compared with the `>` logic operator to the lower bound restriction (February
11th). This comparison alone returns a logic vector (`TRUE`/`FALSE`) of the same
length as `tweets$date`. In conjunction with a second comparison of the upper
bound (February 17th) and the `&` operator, the resulting logic vector is `TRUE`
if the date of one post lies within the specified boundaries and `FALSE` if
not. Using the parenthesis indexing `[row, col]`, this vector can be used to
choose the only those observations (rows) for which the value in the vector is
`TRUE`.

Similarly, parenthesis indexing is used with the `tweets$protected` logic
vector, indicating "private posts", that are not meant to be seen by everybody
and hence should be taken out of the dataset. The `!` operator is used to
reverse the logic operator (`TRUE <-> FALSE`), so observations that are **not**
protected will be chosen.

The dataset is now finished and contains every aspect to be analyzed later
on. If only Twitter-Data shall be used, the reader can skip down to the
[Exporting-Section](#exporting-datasets).

* * *

## Fediverse

The
[Mastodon-API](https://github.com/tootsuite/documentation/blob/461a17603504811b786084176c65f31ae405802d/Using-the-API/API.md)
doesn't require authentication for public timelines or (hash-) tags. Since this
is exactly the data useful to the analysis in question, authentication is not
needed here.

### Scraping Toots and Postings

Contrary to Twitter, Mastodon does not allow to search for a string contained in
posts, however global posts can be searched for specific hashtags through the
tag-timeline in the
[API](https://github.com/tootsuite/documentation/blob/461a17603504811b786084176c65f31ae405802d/Using-the-API/API.md#timelines).
For this, a URL for the API-call has to be built (an eye has to be kept on
changes to the API with according adaptation) in the following form:

```
  https://DOMAIN.OF.INSTANCE/api/v1/timeline/tag/SEARCHTERM
```

Naturally, such API call will not return **all** fitting posts at once right
away. There is a certain limit of posts returned on each call. The maximum can
be derived from the
[documentation](https://github.com/tootsuite/documentation/blob/461a17603504811b786084176c65f31ae405802d/Using-the-API/API.md).
A suitable cap, proven to be working reliably is 40 in the case of
[mastodon.social](https://mastodon.social/). When using other instances, one may
have to adapt. The limit can be specified in the API-call with the `?limit=40`
argument. For the search term it makes sense to use the FSFE's official hashtag
for the "I Love Free Software" Campaign: *ilovefs*.

Building the API-call happens automatically in the custom
[`mastodon.search()`](#mastodonsearch) function, by giving the `instance` and
`hashtag` arguments to the function. However, it can also be done manually if
needed with the `paste()` or `paste0()` function (the latter won't insert empty
spaces between each part and can be though of as an alias of `paste(sep = "")`):

```{.R}
  mastodon_instance <- "https://mastodon.social"
  mastodon_hashtag <- "ilovefs"
  mastodon_url <- paste0(mastodon_instance,
                         "/api/v1/timelines/tag/",
                         mastodon_hashtag,
                         "?limit=40")
```

Internally, the custom [`mastodon.search()`](#mastodonsearch) uses
`curl_fetch_memory()`, `rawToChar()` and `fromJSON()` to fetch the data and
collects the needed information via the custom
[`mastodon.extract()`](#mastodonextract) function. For the end-user this is as
simple as:

```{.R}
  toots <- mastodon.search(hashtag = "ilovefs",
                           instance = "https://mastodon.social")
```

Some (simplified!) *anonymization* of the restulting `toots` object can be done
by simply transforming the `factor()` variables `acct` and `link` to numeric
values, giving them an index number only unique to this very dataset.

Secondly, some minor data-cleaning can be done via the `within()`, `gsub()` and
`strptime()` functions. The `within()` function thereby is mere convenience. It
takes a single dataset as input, as well as expressions to operate *within* the
dataset. This saves the user from repeatedly using the prefix `toots$` in front
of every action. Instead of `toots$acct`, while operating with the `within()`
function, simply `acct` can be used. This way, the `date` and `time` variables
are turned into `character()` values, the *full date* variable `fdat` is created
with `strptime()` and the name of the used instance `inst` is cleaned of
unnecessary information with `gsub()` and some regex.

*If not familiar with regex, [regexr.com](https://regexr.com/) is highly
recommended to learn how to use it. It also contains a nifty cheat-sheet.*

Lastly, it makes sense to restrict the data to a specific time frame (for
example, in order to "filter out" #iLoveFS promotion prior to February
14th). This process can be done with parenthesis indexing, using a logic
operation within:

```{.R}
  toots <- toots[(as.Date(toots$date) > as.Date("2019-02-11") &
                  as.Date(toots$date) < as.Date("2019-02-17")),]
```

The `toots$date` vector is formed to a date object by `as.Date()` and being
compared with the `>` logic operator to the lower bound restriction (February
11th). This comparison alone returns a logic vector (`TRUE`/`FALSE`) of the same
length as `toots$date`. In conjunction with a second comparison of the upper
bound (February 17th) and the `&` operator, the resulting logic vector is `TRUE`
if the date of one post lies within the specified boundaries and `FALSE` if
not. Using the parenthesis indexing `[row, col]`, this vector can be used to
choose the only those observations (rows) for which the value in the vector is
`TRUE`.

The dataset is now finished and contains every aspect to be analyzed later
on. If only Twitter-Data shall be used, the reader can skip down to the
[Exporting-Section](#exporting-datasets).

* * *

## Reddit

*RedditExtractoR (or actually Reddit itself) doesn't currently require users to
authenticate in order to collect data from the platform. As with Mastodon, one
can get directly start scraping.*

### Scraping Posts

There are multiple ways of searching for a certain string. Optionally, one can
determine which Subreddit shall be searched in. In most cases, it makes sense to
search in all of them. To search for a particular string, the function
`reddit_urls()` can be used. It takes one mandatory and five optional arguments:

* the string to searched for (This only looks at titles, not the actual
  content/text of a post). In the case of this analysis, **ilovefs** should work
  just fine, as this is the name of the campaign and probably what redditors
  will use in their posts.
* the subreddits to be searched in. There is no real reason to limit this in the
  case of the ILoveFS-Campaign, but it may make sense in other cases. If not
  needed, this argument can be commented out with a `#`.
* the minimum number of comments a post should have in order to be included. As
  all posts shall be collected, regardless of their popularity, this should be
  set to `0`.
* how many pages of posts the result should include. Here applies the same as
  before: all posts shall be collected, so this option can be set to a very high
  number like `99999`.
* the sort order of the results. This option only really matters if less than
  *all* posts will be collected. Hence it can be left alone at `new`.
* the wait time between API-requests. The minimum (API limit) is 2 seconds, but
  one may be inclinded to use a higher number if problems arise.

The result is saved to the variable `reddit_post_dirty`, where as the *dirty*
stands for the fact that this is unsorted data, probably including posts from
previous years:

```{.R}
  reddit_post_dirty <- reddit_urls(search_terms = "ilovefs",
                                   #subreddit = "freesoftware linux opensource",
                                   cn_threshold = 0,
                                   page_threshold = 99999,
                                   sort_by = "new",
                                   wait_time = 5)
```

### Stripping out data

The data from the `RedditExtractoR` package comes in an easily usable
`data.frame()` output. Its structure is illustrated below:

```
  reddit_post
    |
    |- date = "14-02-17", "13-02-17", ...
    |- num_comments = "23", "15", ...
    |- title = "Why I love Opensource #ilovefs", "Please participate in ILoveFS", ...
    |- subreddit = "opensource", "linux", ...
    '- URL = "https://www.reddit.com/r/opensource/comments/dhfiu/", ...
```

Firstly, the data can be *cleaned* via the `within()` function. In the case of
the reddit data, there is only some variable-renaming and the preparation of
exclusion-factors necessary. The later can then be used to *exclude* posts from
previous years with indexing:

```{.R}
  reddit <- reddit[as.numeric(reddit$year) < 2019,]
```

For the remaining posts, additional data can be fetched with the
`reddit_content()` function, which takes the URL of a post as argument. Hence,
`lapply()` is used to run through each value in the `reddit$link` variable and
fetch additional information like post score (upvotes), comment-text and their
authors. The resulting `list()`

```{.R}
  rdcnt <- lapply(X = reddit$link, FUN = function(x){
      reddit_content(URL = x, wait_time = 30)
  })
```

The resulting `list()` object `rdcnt` contains all according information for
every i-th post. A simple for-loop can be used to assign them to their according
variable in the `reddit` object:

```{.R}
  reddit$ptns <- reddit$text <- reddit$user <- NA
  for(i in 1:length(rdcnt)){
      reddit$ptns[i] <- rdcnt[[i]]$post_score[1]
      reddit$text[i] <- rdcnt[[i]]$post_text[1]
      reddit$user[i] <- rdcnt[[i]]$author[1]
  }
```

The dataset can now be exported (See: [Exporting-Section](#exporting-datasets)).

* * *

## Exporting Datasets

There are several reasons why an archive of the scraped data should be kept:

1. As already mentioned in the [Twitter-Section](#twitter), the social media
   sites do not always enable users to collect a full back-log of what has been
   posted in the past. If trends of participation are of interest, it makes
   sense to have some *old* data in place to compare it to.
2. to use the data outside your current R-session. The variables only live for
   as long as the R-session is running. As soon as it is closed, all is gone.
   So it makes sense to export the data, which then can be imported and worked
   with later again.
3. to enable other users to analyze and work with the data. Obviously, this is
   an important one. The data should be shared, so other people can learn from
   it. Additionally, this makes the analysis transparent instead of simply
   claiming some result.

In order to fully enable anyone to use the data, whatever software is in use,
several data types come in question and are being used here: `.RData .csv .txt
.ods`. The `.txt` format one is the simplest one and can be read by literally
**any** text-editor. Each string is enclosed by quotes `"` and seperated with a
single space in a table-layout. The `.csv` format is very similar, though the
seperation is done with a symbol - in this case a colon `,`.  This format is not
only readable by all text-editors (because it is pure text), it can also be read
by spreadsheet applications like libreoffice-calc. Additionally, data will be
exported as `.ods` (or "Open Document Spreadsheet") to be used by any
spreadsheet editor. The disadvantage of either format is, that they can only
hold items with the same "labels" (i.e. only one `data.frame()` object), so
multiple export-files for each data source have to be created.

Lastly, since this analysis uses R, it makes sense to also export as `.RData`,
R's very own format. `.RData` is a binary format and can not be read by
text-editors or non-specialized software, however the Free Software nature of R
makes it virtually universal in the statistics realm.

In order to create a valid archive of data, the filename should contain the
current date. For this the `Sys.time()` function comes in handy, which is used
in conjunction with `sub()` and `gsub()` to have a clean string of the current
date and time:

```{.R}
  time_of_saving <- sub(x = Sys.time(), pattern = " CET", replace = "") %>%
      sub(pattern = " ", replace = "_") %>%
      gsub(pattern = ":", replace = "-")
```

The `%>%` is a pipe-operator coming from the `tidyr` package. It simply forwards
the results of a command to another one.

Afterwards, the save-path (or file-path) can be defined. In this case, it is
relative to the current working directory: `../data/`. In here, the `.RData`
file contains **all** `data.frame()` objects that were scraped. The `paste0()`
or `paste()` command can be used to create such a file-path. The `save()`
command is used to export the datasets:

```{.R}
  save_path <- paste0("../data/ilovefs-all_", time_of_saving, ".RData")
  save(list = c("tweets", "toots", "reddit"), file = save_path)
```

A similar approach is suitable for the other file formats, however as mentioned
already, every object needs its own file. So the `paste0()` command is used in a
way to create a *vector of paths*, rather than a single value. The appropriate
export commands `write.table()`, `write.csv()` and `write_ods()` can then be
used to export the approriate objects:

```{.R}
  # Save as Text -------------------------
  save_path <- paste0("../data/ilovefs-",c("fediverse_", "twitter_", "reddit_"),
                      time_of_saving, ".txt")
  write.table(x = toots, file = save_path[1])
  write.table(x = tweets, file = save_path[2])
  write.table(x = reddit, file = save_path[3])
  
  # Save as CSV --------------------------
  save_path <- paste0("../data/ilovefs-",c("fediverse_", "twitter_", "reddit_"),
                      time_of_saving, ".csv")
  write.csv(x = toots, file = save_path[1])
  write.csv(x = tweets, file = save_path[2])
  write.csv(x = reddit, file = save_path[3])
  
  # Save as ODS --------------------------
  save_path <- paste0("../data/ilovefs-",c("fediverse_", "twitter_", "reddit_"),
                      time_of_saving, ".ods")
  write_ods(x = toots, path = save_path[1])
  write_ods(x = tweets, path = save_path[2])
  write_ods(x = reddit, path = save_path[3])
```

**If this is done, the R-Session can safely be closed!**

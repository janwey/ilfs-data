#  Copyright (c) 2018 Free Software Foundation Europe e.V. <contact@fsfe.org>  \
#  Author 2018 Vincent Lequertier <vincent@fsfe.org>                           \
#  SPDX-License-Identifier: GPL-3.0


img:
	python word_cloud.py | wordcloud_cli.py --relative_scaling 0.6 --imagefile graphics/word_cloud.png --width=2000 --height=2000 --no_collocations --background="#ffffff"
